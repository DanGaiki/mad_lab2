package com.mad.lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.system.Os.close
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var detailsFragment : DetailsFragment
    lateinit var editorItemFragment : EditorItemFragment
    lateinit var salesFragment : SalesFragment
    lateinit var userProfileFragment: UserProfileFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolBar)
        val actionBar = supportActionBar
        actionBar?.title = "Navigation Drawer"
        val drawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle (
            this,
            drawerLayout,
            toolBar,
            (R.string.open),
            (R.string.close)
        ){

        }
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        nav_view.setNavigationItemSelectedListener (this)
            //navigation item selected listener
            // the default fragment is Sales Fragment
        salesFragment = SalesFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout, salesFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
            //fragment code
    }
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        //related fragments code
        when (menuItem.itemId){
            R.id.items_sale -> {
                salesFragment = SalesFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, salesFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //fragment code
            }
            R.id.item_details -> {
                detailsFragment = DetailsFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, detailsFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //fragment code
            }
            R.id.item_create_modify -> {
                editorItemFragment = EditorItemFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, editorItemFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //fragment code
            }
            R.id.user_profile_menu -> {
                userProfileFragment = UserProfileFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_layout, userProfileFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
                //fragment code
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        else {
            super.onBackPressed()
        }
    }
}
